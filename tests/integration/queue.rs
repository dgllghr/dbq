use super::*;

#[test]
fn test_single_job() {
    let schema_config = init();
    let queue_name = "test_single_job";
    let queue = dbq::Queue::new(schema_config, queue_name.to_string());

    let conn = db_conn();
    queue.clear(&conn).unwrap();
    let job_id = enqueue_test_job(&queue, 1, &conn);
    let len = queue.len(&conn).unwrap();
    assert_eq!(1, len);
    let job = queue.lookup_in_queue(job_id, &conn).unwrap().unwrap();
    assert_eq!("foo", job.class);
    assert_eq!(1, job.max_attempts);
    queue.clear(&conn).unwrap();
    let len = queue.len(&conn).unwrap();
    assert_eq!(0, len);
}

#[test]
fn test_multiple_jobs() {
    let schema_config = init();
    let queue_name = "test_multiple_jobs";
    let queue = dbq::Queue::new(schema_config, queue_name.to_string());
    let num_jobs = 10;

    let conn = db_conn();
    queue.clear(&conn).unwrap();
    let mut job_ids = Vec::new();
    for _ in 0..num_jobs {
        let job_id = enqueue_test_job(&queue, 1, &conn);
        job_ids.push(job_id);
    }
    let len = queue.len(&conn).unwrap();
    assert_eq!(num_jobs, len);
    for job_id in job_ids.iter() {
        let job = queue.lookup_in_queue(*job_id, &conn).unwrap().unwrap();
        assert_eq!("foo", job.class);
        assert_eq!(1, job.max_attempts);
    }
    queue.clear(&conn).unwrap();
    let len = queue.len(&conn).unwrap();
    assert_eq!(0, len);
}
