#!/usr/bin/env bash

# Print commands
set -x

# Create the integration test environment
docker-compose -f tests/docker-compose.yml up &

# Wait for the environment to be ready
sleep 10

# Run the tests
export DBQ_POSTGRES_HOST="localhost"
cargo test --features integration_tests
exit_code=$?

# Clean up
docker-compose -f tests/docker-compose.yml down

# Exit with the exit code from the tests
exit ${exit_code}