#[cfg(feature = "integration_tests")]
extern crate dbq;

#[cfg(feature = "integration_tests")]
mod integration {
    mod queue;
    mod worker;

    use postgres::params::{Builder, ConnectParams, Host};
    use postgres::{Connection, GenericConnection, TlsMode};
    use serde_json::Value;
    use std::time::Duration;

    pub fn init() -> dbq::SchemaConfig {
        let _ = env_logger::try_init();
        let schema_config = dbq::SchemaConfig::default();
        let conn = Connection::connect(db_connect_params(), TlsMode::None).unwrap();
        dbq::run_migrations(&schema_config, &conn, Some(646_271)).unwrap();
        schema_config
    }

    pub fn db_connect_params() -> ConnectParams {
        let mut builder = Builder::new();
        builder.user("postgres", Some("password"));
        builder.database("dbq");
        builder.connect_timeout(Some(Duration::new(10, 0)));
        let host = std::env::var("DBQ_POSTGRES_HOST")
            .ok()
            .unwrap_or("localhost".to_string());
        builder.build(Host::Tcp(host))
    }

    pub fn db_conn() -> Connection {
        let conn = Connection::connect(db_connect_params(), TlsMode::None).unwrap();
        conn
    }

    pub fn enqueue_test_job<C: GenericConnection>(
        queue: &dbq::Queue,
        max_attempts: u32,
        conn: &C,
    ) -> u64 {
        let args = r#"
            {
                "id": 1,
                "message": "hello"
            }"#;
        let v: Value = serde_json::from_str(args).unwrap();
        queue.enqueue("foo", &v, max_attempts, conn).unwrap()
    }
}
