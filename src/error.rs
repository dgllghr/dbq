use postgres;
use std::error::Error as StdError;
use std::fmt;
use std::io;

/// Result type for `dbq::Error`
pub type Result<T> = std::result::Result<T, Error>;

/// Wrapper type for errors that may occur in `dbq`
#[derive(Debug)]
pub enum Error {
    Any(Box<dyn std::any::Any + Send + 'static>),
    Generic(Box<StdError + Send + Sync + 'static>),
    Io(io::Error),
    Postgres(postgres::Error),
    Json(serde_json::Error),
}

impl StdError for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Any(err) => write!(f, "any error. {:?}", err),
            Error::Generic(err) => write!(f, "generic error. {}", err),
            Error::Io(err) => write!(f, "io error. {}", err),
            Error::Postgres(err) => write!(f, "postgres error. {}", err),
            Error::Json(err) => write!(f, "json error. {}", err),
        }
    }
}

impl From<Box<StdError + Send + Sync + 'static>> for Error {
    fn from(err: Box<StdError + Send + Sync + 'static>) -> Error {
        Error::Generic(err)
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<postgres::Error> for Error {
    fn from(err: postgres::Error) -> Error {
        Error::Postgres(err)
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Error {
        Error::Json(err)
    }
}
