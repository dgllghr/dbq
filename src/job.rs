use chrono::{DateTime, Utc};
use serde_json::Value;

/// Specification and status of a job
#[derive(Clone, Debug)]
pub struct Job {
    /// Unique identifier for the job within the queue
    pub id: u64,

    /// Queue the job is part of
    pub queue: String,

    /// Type of job
    pub class: String,

    /// JSON arguments to the job
    pub args: Value,

    /// Total number of times the job may be run before being moved to "dead
    /// letters" storage and no longer retried
    pub max_attempts: u32,

    /// Instant when the job was created
    pub created_at: DateTime<Utc>,

    /// Earliest instant when the job may be run.
    pub run_next_at: Option<DateTime<Utc>>,

    /// Instant when the last run failed. This only ever has a non-`None` value
    /// when the job has been run and failed previously.
    pub last_failed_at: Option<DateTime<Utc>>,

    /// Error message from last failed run. This only ever has a non-`None`
    /// value when the job has been run and failed previously.
    pub last_error: Option<String>,

    /// Number of times the job has been run and failed
    pub error_count: u32,
}

impl Job {
    /// If true, the job will not be retried after the current attempt and will
    /// be moved to "dead letters" storage if it fails. If false, the job will
    /// be retried on failure.
    pub fn final_attempt(&self) -> bool {
        self.error_count == self.max_attempts - 1
    }

    /// If true, the job has been attempted at least `max_attempts` times and
    /// is no longe being retried. If false, the job may have succeeded or will
    /// contintue to be retried on failure
    pub fn failed(&self) -> bool {
        self.error_count >= self.max_attempts
    }
}
